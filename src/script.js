import $ from 'jquery'
import {
    MDCTopAppBar
} from '@material/top-app-bar'
import {
    MDCTabBar
} from '@material/tab-bar'


$(() => {
    new MDCTopAppBar($('.mdc-top-app-bar')[0])
    new MDCTabBar($('.mdc-tab-bar')[0])
})